package org.elmis.web.services.updater;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.elmis.web.domain.dao.UpdateFeedbackDaoImpl;
import org.elmis.web.domain.model.UpdateFeedback;
import org.hibernate.HibernateException;

@Path("/feedback")
public class FeedbackUpdater {

	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	//@Consumes(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	public UpdateFeedback createFeedback(UpdateFeedback updateFeedback){
		System.out.println(updateFeedback);
		try{
			new UpdateFeedbackDaoImpl().addFeedback(updateFeedback);
		}
		catch(HibernateException e){
			e.printStackTrace();
		}
		return updateFeedback;
	}
	@GET
	@Path("/fetch")
	@Produces({ MediaType.TEXT_HTML})
	public String getFeedback(){
		StringBuilder sb = new StringBuilder();
		sb.append("<html><body>");
		sb.append("<table><tr><td>id</td><td>Update Version</td><td>Computer Name</td></tr>");
		try{
			List<UpdateFeedback> feedbackList = new UpdateFeedbackDaoImpl().getUpdateFeedback();
			for (UpdateFeedback fb : feedbackList){
				sb.append("<tr><td>");
				sb.append(""+fb.getId()+"</td><td>");
				sb.append(""+fb.getUpdateVersion()+"</td><td>");
				sb.append(fb.getComputerName()+"</td></tr>");
			}
			sb.append("</table></body></htm>");

		}
		catch(HibernateException e){
			e.printStackTrace();	
		}
		return sb.toString();
	}
}
