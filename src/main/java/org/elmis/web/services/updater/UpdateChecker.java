package org.elmis.web.services.updater;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/update")
public class UpdateChecker {

	private static final String update = ""+Updater.getLastUpdate("D:/test", ".sql");
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getUpdateVersion(){
		return update;
	}
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String getUpdateVersionHTMLPage(){
		return "<html><title>Version "+update+"</title><body>Version "+update+" is available for download</body></html>";
	}
}
