package org.elmis.web.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class WebAppUtil {

	public Properties getSqliteProperties()
	{
		Properties props = new Properties();
		try{//FileInputStream fis = new FileInputStream("resources/sqlite.properties"); 
		InputStream fis = this.getClass().getResourceAsStream("sqlite.properties");
			//props = new Properties(System.getProperties());
			props.load(fis);
			//System.setProperties(props);
			return props;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return props;
	}
}
