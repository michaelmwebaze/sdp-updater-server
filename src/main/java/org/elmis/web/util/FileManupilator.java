package org.elmis.web.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class FileManupilator {

	
	public static void copyUpdate(String updateLocation, String destinationLocation) throws IOException{
		FileUtils.copyFile(FileUtils.getFile(updateLocation),FileUtils.getFile(destinationLocation));
	}
	public static String getExtensionType(String fileLocation){

		return FilenameUtils.getExtension(fileLocation);
	}
	public static String getBaseName(String fileLocation){
		return FilenameUtils.getBaseName(fileLocation);
	}
	public static void listDirectoryContents(String directoryLocation) throws IOException{


	}
	public static void decompressFile(String dirLocation) throws IOException{
		File dir = new File(dirLocation);
		List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		for (File file : files) {
			String ext = getExtensionType(file.getCanonicalPath());

			if (ext.equalsIgnoreCase("zip")){
				byte[] buffer = new byte[1024];

				ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(file.getCanonicalPath()));
				ZipEntry zipEntry = zipInputStream.getNextEntry();

				while(zipEntry != null){
					String fileName = zipEntry.getName();
					File newFile = new File("download" + File.separator + fileName);

					System.out.println("file unzip : "+ newFile.getAbsoluteFile());

					new File(newFile.getParent()).mkdirs();

					FileOutputStream fos = new FileOutputStream(newFile);             

					int len;
					while ((len = zipInputStream.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}

					fos.close();   
					zipEntry = zipInputStream.getNextEntry();
				}
				zipInputStream.closeEntry();
				zipInputStream.close();

				System.out.println("Uncompressing complete");
			}
		}
	}
	public static void compressFile(String dirLocation){
		byte[] buffer = new byte[1024];
		File dir = new File(dirLocation);
		List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);

		try{
			FileOutputStream fos = new FileOutputStream("E:/updates/updatersds.zip");
			ZipOutputStream zos = new ZipOutputStream(fos);

			for (File file : files) {
				ZipEntry ze= new ZipEntry(file.getCanonicalPath());
				zos.putNextEntry(ze);
				
				FileInputStream in = 
						new FileInputStream( File.separator + file);

				int len;
				while ((len = in.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}

				in.close();
			}
			zos.closeEntry();
	    	zos.close();

		}
		catch(IOException e){
			System.out.println(e.getLocalizedMessage());
		}
	}
	public static void createDirectory(String directoryName){
		File folder = new File(directoryName);
		if(!folder.exists()){
    		folder.mkdir();
    	}
	}
	public static int getLastFile(String directory, String ext){
		// Directory path here
		//String path = "."; 
		List<Integer> list = new ArrayList<>();

		String files;
		File folder = new File(directory);
		File[] listOfFiles = folder.listFiles(); 

		for (int i = 0; i < listOfFiles.length; i++) 
		{

			if (listOfFiles[i].isFile()) 
			{
				files = listOfFiles[i].getName();
				if (files.endsWith(ext) || files.endsWith(ext))
				{
					String fileName = listOfFiles[i].getName();
					
					//Integer.parseInt(fileName)
					String fileNameWithOutExt = FilenameUtils.removeExtension(listOfFiles[i].getName());
					Integer f = Integer.parseInt(fileNameWithOutExt);
					System.out.println(f);
					
					list.add(f);
					System.out.println(files);
				}
			}
		}
		Collections.sort(list);
		System.out.println( Arrays.asList(list ));
		return list.get(list.size() - 1);
	}
}
