package org.elmis.web.database;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.elmis.web.util.WebAppUtil;
/**
 * MyBatisConnectionFactory.java
 * Purpose: Manages all connection resources (Opening & closing) database(s). 
 * @author Michael Mwebaze
 *@version 1.0
 */
public class MyBatisConnectionFactory {

	private static SqlSessionFactory sqlSessionFactory;

	static {
		try {

			System.out.println("SQLITE DATABASE...");

			String resource = "mybatis-config.xml";
System.out.println("******");
			Reader reader = Resources.getResourceAsReader(resource);
System.out.println("XXXXXXXX");
			if (sqlSessionFactory == null) {
				sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader, new WebAppUtil().getSqliteProperties());
			}
System.out.println("EEEEE");
		}
		catch (FileNotFoundException fileNotFoundException) {
			System.out.println("**** -> "+fileNotFoundException.getLocalizedMessage());
			fileNotFoundException.printStackTrace();
		}
		catch (IOException iOEx) {
			System.out.println("MyBATIS DB CON ISSUES "+iOEx.getMessage());
		}
	}

	/**
	 * 
	 * @return an SqlSessionFactory which is used to open or close a database connection resource.
	 * Each open SqlSessionFactory that is opened must after use be closed
	 */
	public static SqlSessionFactory getSqlSessionFactory() {

		return sqlSessionFactory;
	}
}