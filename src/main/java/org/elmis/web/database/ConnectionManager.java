package org.elmis.web.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.elmis.web.util.WebAppUtil;
/**
 * 
 * @author Michael Mwebaze
 * Singleton class to manage connection pools
 */
public class ConnectionManager {

	private static ConnectionManager instance = null;
	
	private static final String USERNAME = "dbuser";
	private static final String PASSWORD = "dbpassword";
	//private static final String CONN_STRING = "jdbc:hsqldb:database/elmis_facility";
	private String CONNECTIONSTRING; //= UpdaterProperties.getSqliteProperties().getProperty("dburl");
	private DBType dbType = DBType.SQLITE;
	private Connection conn = null;
	
	private ConnectionManager()
	{
		
	}
	
	public static ConnectionManager getInstance()
	{
		if (instance == null)
		{
			instance = new ConnectionManager();
		}
		return instance;
	}
	
	/**
	 *set the type of database to connect too. At the moment supports MYSQL ONLY 
	 * @param dbType
	 */
	public void setDBType(DBType dbType)
	{
		this.dbType = dbType;
	}
	private boolean openConnection() throws SQLException, ClassNotFoundException
	{
		switch (dbType) {
		case SQLITE:
			//Class.forName("com.mysql.jdbc.Driver");
			CONNECTIONSTRING = new WebAppUtil().getSqliteProperties().getProperty("dburl");
			conn = DriverManager.getConnection(CONNECTIONSTRING, USERNAME, PASSWORD);
			System.out.println("Connected to SQLITE");
			return true;
			
		/*case POSTGRESQL:
			CONNECTIONSTRING = UpdaterProperties.getPsqlProperties().getProperty("dburl");
			String password = UpdaterProperties.getPsqlProperties().getProperty("dbpassword");
			conn = DriverManager.getConnection(CONNECTIONSTRING, UpdaterProperties.getPsqlProperties().getProperty("dbuser"), AESencrpDecrp.decrypt(password));
			System.out.println("Connected to POSTGRES");
			return true;*/

		default:
			return false;
		}
	}
	
	public  Connection getConnection() throws SQLException, ClassNotFoundException
	{
		if (conn == null) {
			if (openConnection()) {
				return conn;
			}
			else
				return null;
		}
		return conn;
	}
	
	public void close()
	{
		try {
			conn.close();
			conn = null;
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
