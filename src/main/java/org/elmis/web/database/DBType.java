package org.elmis.web.database;

public enum DBType {

	MYSQL, HSQL, ORACLE, MSQL, POSTGRESQL,SQLITE;
}
