package org.elmis.web.domain.dao;

import java.util.List;

import org.elmis.web.domain.model.UpdateFeedback;
import org.elmis.web.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class UpdateFeedbackDaoImpl implements UpdateFeedbackDao{
	private SessionFactory sessionFactory;

	/*private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}*/

	@Override
	public List<UpdateFeedback> getUpdateFeedback() {

		//Hibernate
		Session session = HibernateUtil.getSessionFactory().openSession();
		@SuppressWarnings("unchecked")
		List<UpdateFeedback> listUser = session.createQuery("from UpdateFeedback").list();
		HibernateUtil.shutdown();

		return listUser;

		//MyBatis 
		/*SqlSession session = sqlSessionFactory.openSession(false);
		List<UpdateFeedback> feedbackList = null;
		try{
			return session.selectList("UpdateFeedback.feedback");
		}
		finally{
			session.close();
		}*/
	}

	@Override
	public void addFeedback(UpdateFeedback updateFeedback) throws HibernateException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(updateFeedback);
		session.getTransaction().commit();
		HibernateUtil.shutdown();
	}
}
