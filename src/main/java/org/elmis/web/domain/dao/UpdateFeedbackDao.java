package org.elmis.web.domain.dao;

import java.util.List;

import org.elmis.web.domain.model.UpdateFeedback;

public interface UpdateFeedbackDao {

	public List<UpdateFeedback> getUpdateFeedback();
	public void addFeedback(UpdateFeedback updateFeedback);
}
