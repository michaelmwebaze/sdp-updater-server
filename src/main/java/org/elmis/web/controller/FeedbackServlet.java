package org.elmis.web.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.elmis.web.domain.dao.UpdateFeedbackDao;
import org.elmis.web.domain.dao.UpdateFeedbackDaoImpl;

@WebServlet("/FeedbackServlet")
public class FeedbackServlet extends HttpServlet{
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
	}
	public void doGet(HttpServletRequest request,
			HttpServletResponse response)
					throws ServletException, IOException	{
		
		UpdateFeedbackDao updateFeedback = new UpdateFeedbackDaoImpl();
		System.out.println("size is "+updateFeedback.getUpdateFeedback().size());
	}

}
