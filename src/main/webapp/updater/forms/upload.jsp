<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Upload</title>
</head>
<script type="text/javascript">
	function dothat() {
		var div = document.getElementById('fileuploads');
		var field = div.getElementsByTagName('input')[0];

		div.appendChild(document.createElement("br"));
		div.appendChild(field.cloneNode(false));
	}
</script>
<body>
	<div id="container">
		<nav>
			<a href="../../rest/update">Current Version</a> | <a href="#">Manual
				download</a> |
		</nav>
		<div id="section" align="center">
			<h1>Select File(s) to Upload</h1>
			<form method="post" action="/ServerUpdater/UploadServlet"
				enctype="multipart/form-data">
				<div id="fileuploads">
					<input type="file" name="file" size="60" /><br />
				</div>
				<!-- Select update to upload 2: <input type="file" size="50" /><br />
				Select update to upload 3: <input type="file" size="50" /><br /> <br /> -->
				<input type="button" name="addmore" id="addmore" value="Add More"
					onClick="dothat();" /> <input type="submit" value="Upload" />
			</form>
		</div>
	</div>
</body>
</html>